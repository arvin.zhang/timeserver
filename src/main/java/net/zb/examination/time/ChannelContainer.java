package net.zb.examination.time;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/18, 初始化版本
 * @version 1.0
 **/
public class ChannelContainer {

	private static ChannelGroup channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

	private static final ScheduledExecutorService EXECUTOR = Executors.newScheduledThreadPool(50, r -> {
		Thread t = new Thread(r);
		t.setName("timeSend-" + t.getId());
		/* 打开守护线程 */
		t.setDaemon(true);
		return t;
	});


	static {
		EXECUTOR.scheduleWithFixedDelay(()-> {
			Long now = System.currentTimeMillis();
			channelGroup.writeAndFlush(now + "\r\n");
		}, 0, 1, TimeUnit.SECONDS);
	}

	public static void addChannel(ChannelHandlerContext ctx){
		channelGroup.add(ctx.channel());
	}



	public static void removeChannel(ChannelHandlerContext ctx){
		channelGroup.remove(ctx.channel());
	}






}
