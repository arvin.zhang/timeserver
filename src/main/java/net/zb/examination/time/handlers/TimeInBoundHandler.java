package net.zb.examination.time.handlers;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import net.zb.examination.time.ChannelContainer;

import java.nio.charset.Charset;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/15, 初始化版本
 * @version 1.0
 **/
@Slf4j
public class TimeInBoundHandler extends SimpleChannelInboundHandler<String> {


	private static final Charset UTF8 = Charset.forName("UTF-8");
	private static final int CODE_CONNECT_SUCCESS = 220;


	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		String line = CODE_CONNECT_SUCCESS + " " + "time server (" + 1.0F + ") ready." + "\r\n";
		byte[] data = line.getBytes(UTF8);
		ctx.writeAndFlush(Unpooled.wrappedBuffer(data));
		ChannelContainer.addChannel(ctx);
	}


	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		super.channelInactive(ctx);
		ChannelContainer.removeChannel(ctx);
	}


	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {

	}

}
